-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2020 at 05:00 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibis_full_stack`
--

-- --------------------------------------------------------

--
-- Table structure for table `atmosphere`
--

CREATE TABLE `atmosphere` (
  `id` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `contract` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `atmosphere`
--

INSERT INTO `atmosphere` (`id`, `x`, `y`, `contract`) VALUES
(1, 0, -16, 255),
(2, 10, -2, 255),
(3, 20, 9, 255),
(4, 30, -56, 255),
(5, 40, -5, 255),
(6, 50, -36, 255),
(7, 60, -63, 255),
(8, 70, -25, 255),
(9, 80, -49, 255),
(10, 90, -13, 255),
(11, 0, -51, 311),
(12, 10, 13, 311),
(13, 20, 4, 311),
(14, 30, -51, 311),
(15, 40, -66, 311),
(16, 50, -20, 311),
(17, 60, -42, 311),
(18, 70, -42, 311),
(19, 80, -1, 311),
(20, 90, -4, 311),
(21, 0, -27, 555),
(22, 10, -33, 555),
(23, 20, -24, 555),
(24, 30, -9, 555),
(25, 40, -26, 555),
(26, 50, -70, 555),
(27, 60, -55, 555),
(28, 70, -48, 555),
(29, 80, -19, 555),
(30, 90, -12, 555),
(31, 0, -8, 255),
(32, 10, -42, 255),
(33, 20, 13, 255),
(34, 30, 3, 255),
(35, 40, -68, 255),
(36, 50, -9, 255),
(37, 60, -42, 255),
(38, 70, -13, 255),
(39, 80, -50, 255),
(40, 90, -25, 255),
(41, 0, -38, 311),
(42, 10, 5, 311),
(43, 20, -53, 311),
(44, 30, -13, 311),
(45, 40, -28, 311),
(46, 50, -62, 311),
(47, 60, -69, 311),
(48, 70, 0, 311),
(49, 80, -65, 311),
(50, 90, -43, 311),
(51, 0, -57, 555),
(52, 10, 6, 555),
(53, 20, -12, 555),
(54, 30, -30, 555),
(55, 40, 7, 555),
(56, 50, -14, 555),
(57, 60, -63, 555),
(58, 70, -30, 555),
(59, 80, 5, 555),
(60, 90, -54, 555);

-- --------------------------------------------------------

--
-- Table structure for table `avg_sales`
--

CREATE TABLE `avg_sales` (
  `id` int(11) NOT NULL,
  `label` text NOT NULL,
  `value` int(11) NOT NULL,
  `contract` int(11) NOT NULL,
  `product` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `avg_sales`
--

INSERT INTO `avg_sales` (`id`, `label`, `value`, `contract`, `product`) VALUES
(1, 'ssssss', 255, 25252152, ''),
(2, 'milk', 301, 255, 'January'),
(3, 'honey', 139, 255, 'January'),
(4, 'bread', 94, 255, 'January'),
(5, 'milk', 464, 255, 'February'),
(6, 'honey', 94, 255, 'February'),
(7, 'bread', 71, 255, 'February'),
(8, 'milk', 488, 255, 'March'),
(9, 'honey', 64, 255, 'March'),
(10, 'bread', 359, 255, 'March'),
(11, 'milk', 158, 255, 'April'),
(12, 'honey', 283, 255, 'April'),
(13, 'bread', 477, 255, 'April'),
(14, 'milk', 81, 255, 'May'),
(15, 'honey', 104, 255, 'May'),
(16, 'bread', 387, 255, 'May'),
(17, 'milk', 452, 255, 'June'),
(18, 'honey', 180, 255, 'June'),
(19, 'bread', 335, 255, 'June'),
(20, 'milk', 156, 255, 'July '),
(21, 'honey', 361, 255, 'July '),
(22, 'bread', 370, 255, 'July '),
(23, 'milk', 245, 255, 'August'),
(24, 'honey', 259, 255, 'August'),
(25, 'bread', 470, 255, 'August'),
(26, 'milk', 359, 255, 'September'),
(27, 'honey', 125, 255, 'September'),
(28, 'bread', 160, 255, 'September'),
(29, 'milk', 184, 255, 'October'),
(30, 'honey', 392, 255, 'October'),
(31, 'bread', 131, 255, 'October'),
(32, 'milk', 244, 255, 'November'),
(33, 'honey', 179, 255, 'November'),
(34, 'bread', 383, 255, 'November'),
(35, 'milk', 146, 255, 'December'),
(36, 'honey', 427, 255, 'December'),
(37, 'bread', 375, 255, 'December'),
(38, 'milk', 486, 255, ''),
(39, 'honey', 458, 255, ''),
(40, 'bread', 215, 255, ''),
(41, 'milk', 288, 311, 'January'),
(42, 'honey', 319, 311, 'January'),
(43, 'bread', 127, 311, 'January'),
(44, 'milk', 341, 311, 'February'),
(45, 'honey', 220, 311, 'February'),
(46, 'bread', 88, 311, 'February'),
(47, 'milk', 437, 311, 'March'),
(48, 'honey', 119, 311, 'March'),
(49, 'bread', 348, 311, 'March'),
(50, 'milk', 104, 311, 'April'),
(51, 'honey', 283, 311, 'April'),
(52, 'bread', 319, 311, 'April'),
(53, 'milk', 224, 311, 'May'),
(54, 'honey', 446, 311, 'May'),
(55, 'bread', 478, 311, 'May'),
(56, 'milk', 249, 311, 'June'),
(57, 'honey', 79, 311, 'June'),
(58, 'bread', 340, 311, 'June'),
(59, 'milk', 487, 311, 'July '),
(60, 'honey', 305, 311, 'July '),
(61, 'bread', 472, 311, 'July '),
(62, 'milk', 346, 311, 'August'),
(63, 'honey', 449, 311, 'August'),
(64, 'bread', 149, 311, 'August'),
(65, 'milk', 364, 311, 'September'),
(66, 'honey', 421, 311, 'September'),
(67, 'bread', 150, 311, 'September'),
(68, 'milk', 144, 311, 'October'),
(69, 'honey', 382, 311, 'October'),
(70, 'bread', 378, 311, 'October'),
(71, 'milk', 150, 311, 'November'),
(72, 'honey', 231, 311, 'November'),
(73, 'bread', 148, 311, 'November'),
(74, 'milk', 155, 311, 'December'),
(75, 'honey', 241, 311, 'December'),
(76, 'bread', 379, 311, 'December'),
(77, 'milk', 209, 311, ''),
(78, 'honey', 452, 311, ''),
(79, 'bread', 406, 311, ''),
(80, 'milk', 418, 555, 'January'),
(81, 'honey', 390, 555, 'January'),
(82, 'bread', 220, 555, 'January'),
(83, 'milk', 114, 555, 'February'),
(84, 'honey', 176, 555, 'February'),
(85, 'bread', 407, 555, 'February'),
(86, 'milk', 94, 555, 'March'),
(87, 'honey', 452, 555, 'March'),
(88, 'bread', 292, 555, 'March'),
(89, 'milk', 473, 555, 'April'),
(90, 'honey', 347, 555, 'April'),
(91, 'bread', 313, 555, 'April'),
(92, 'milk', 375, 555, 'May'),
(93, 'honey', 388, 555, 'May'),
(94, 'bread', 237, 555, 'May'),
(95, 'milk', 350, 555, 'June'),
(96, 'honey', 120, 555, 'June'),
(97, 'bread', 321, 555, 'June'),
(98, 'milk', 188, 555, 'July '),
(99, 'honey', 268, 555, 'July '),
(100, 'bread', 233, 555, 'July '),
(101, 'milk', 339, 555, 'August'),
(102, 'honey', 466, 555, 'August'),
(103, 'bread', 415, 555, 'August'),
(104, 'milk', 329, 555, 'September'),
(105, 'honey', 263, 555, 'September'),
(106, 'bread', 184, 555, 'September'),
(107, 'milk', 385, 555, 'October'),
(108, 'honey', 243, 555, 'October'),
(109, 'bread', 102, 555, 'October'),
(110, 'milk', 377, 555, 'November'),
(111, 'honey', 457, 555, 'November'),
(112, 'bread', 358, 555, 'November'),
(113, 'milk', 404, 555, 'December'),
(114, 'honey', 84, 555, 'December'),
(115, 'bread', 454, 555, 'December'),
(116, 'milk', 483, 555, ''),
(117, 'honey', 453, 555, ''),
(118, 'bread', 355, 555, ''),
(119, 'milk', 290, 255, 'January'),
(120, 'honey', 161, 255, 'January'),
(121, 'bread', 182, 255, 'January'),
(122, 'milk', 246, 255, 'February'),
(123, 'honey', 305, 255, 'February'),
(124, 'bread', 419, 255, 'February'),
(125, 'milk', 259, 255, 'March'),
(126, 'honey', 454, 255, 'March'),
(127, 'bread', 78, 255, 'March'),
(128, 'milk', 246, 255, 'April'),
(129, 'honey', 500, 255, 'April'),
(130, 'bread', 325, 255, 'April'),
(131, 'milk', 424, 255, 'May'),
(132, 'honey', 471, 255, 'May'),
(133, 'bread', 123, 255, 'May'),
(134, 'milk', 91, 255, 'June'),
(135, 'honey', 274, 255, 'June'),
(136, 'bread', 131, 255, 'June'),
(137, 'milk', 264, 255, 'July '),
(138, 'honey', 191, 255, 'July '),
(139, 'bread', 129, 255, 'July '),
(140, 'milk', 117, 255, 'August'),
(141, 'honey', 96, 255, 'August'),
(142, 'bread', 282, 255, 'August'),
(143, 'milk', 151, 255, 'September'),
(144, 'honey', 148, 255, 'September'),
(145, 'bread', 166, 255, 'September'),
(146, 'milk', 167, 255, 'October'),
(147, 'honey', 470, 255, 'October'),
(148, 'bread', 212, 255, 'October'),
(149, 'milk', 248, 255, 'November'),
(150, 'honey', 457, 255, 'November'),
(151, 'bread', 374, 255, 'November'),
(152, 'milk', 143, 255, 'December'),
(153, 'honey', 262, 255, 'December'),
(154, 'bread', 102, 255, 'December'),
(155, 'milk', 148, 255, ''),
(156, 'honey', 56, 255, ''),
(157, 'bread', 449, 255, ''),
(158, 'milk', 131, 311, 'January'),
(159, 'honey', 250, 311, 'January'),
(160, 'bread', 234, 311, 'January'),
(161, 'milk', 180, 311, 'February'),
(162, 'honey', 341, 311, 'February'),
(163, 'bread', 461, 311, 'February'),
(164, 'milk', 422, 311, 'March'),
(165, 'honey', 193, 311, 'March'),
(166, 'bread', 221, 311, 'March'),
(167, 'milk', 217, 311, 'April'),
(168, 'honey', 329, 311, 'April'),
(169, 'bread', 130, 311, 'April'),
(170, 'milk', 272, 311, 'May'),
(171, 'honey', 54, 311, 'May'),
(172, 'bread', 215, 311, 'May'),
(173, 'milk', 291, 311, 'June'),
(174, 'honey', 376, 311, 'June'),
(175, 'bread', 70, 311, 'June'),
(176, 'milk', 338, 311, 'July '),
(177, 'honey', 372, 311, 'July '),
(178, 'bread', 311, 311, 'July '),
(179, 'milk', 235, 311, 'August'),
(180, 'honey', 114, 311, 'August'),
(181, 'bread', 223, 311, 'August'),
(182, 'milk', 376, 311, 'September'),
(183, 'honey', 394, 311, 'September'),
(184, 'bread', 95, 311, 'September'),
(185, 'milk', 82, 311, 'October'),
(186, 'honey', 164, 311, 'October'),
(187, 'bread', 305, 311, 'October'),
(188, 'milk', 176, 311, 'November'),
(189, 'honey', 101, 311, 'November'),
(190, 'bread', 288, 311, 'November'),
(191, 'milk', 54, 311, 'December'),
(192, 'honey', 466, 311, 'December'),
(193, 'bread', 267, 311, 'December'),
(194, 'milk', 189, 311, ''),
(195, 'honey', 290, 311, ''),
(196, 'bread', 424, 311, ''),
(197, 'milk', 68, 555, 'January'),
(198, 'honey', 275, 555, 'January'),
(199, 'bread', 457, 555, 'January'),
(200, 'milk', 444, 555, 'February'),
(201, 'honey', 154, 555, 'February'),
(202, 'bread', 120, 555, 'February'),
(203, 'milk', 412, 555, 'March'),
(204, 'honey', 247, 555, 'March'),
(205, 'bread', 114, 555, 'March'),
(206, 'milk', 98, 555, 'April'),
(207, 'honey', 236, 555, 'April'),
(208, 'bread', 83, 555, 'April'),
(209, 'milk', 350, 555, 'May'),
(210, 'honey', 65, 555, 'May'),
(211, 'bread', 63, 555, 'May'),
(212, 'milk', 152, 555, 'June'),
(213, 'honey', 378, 555, 'June'),
(214, 'bread', 315, 555, 'June'),
(215, 'milk', 345, 555, 'July '),
(216, 'honey', 400, 555, 'July '),
(217, 'bread', 151, 555, 'July '),
(218, 'milk', 120, 555, 'August'),
(219, 'honey', 164, 555, 'August'),
(220, 'bread', 66, 555, 'August'),
(221, 'milk', 237, 555, 'September'),
(222, 'honey', 65, 555, 'September'),
(223, 'bread', 199, 555, 'September'),
(224, 'milk', 309, 555, 'October'),
(225, 'honey', 214, 555, 'October'),
(226, 'bread', 70, 555, 'October'),
(227, 'milk', 241, 555, 'November'),
(228, 'honey', 290, 555, 'November'),
(229, 'bread', 330, 555, 'November'),
(230, 'milk', 415, 555, 'December'),
(231, 'honey', 457, 555, 'December'),
(232, 'bread', 303, 555, 'December'),
(233, 'milk', 305, 555, ''),
(234, 'honey', 389, 555, ''),
(235, 'bread', 203, 555, '');

-- --------------------------------------------------------

--
-- Table structure for table `avg_temp`
--

CREATE TABLE `avg_temp` (
  `id` int(11) NOT NULL,
  `label` text NOT NULL,
  `value` float NOT NULL,
  `contract` int(11) NOT NULL,
  `month` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `avg_temp`
--

INSERT INTO `avg_temp` (`id`, `label`, `value`, `contract`, `month`) VALUES
(1, 'London', 0, 255, 'January'),
(2, 'London', 33, 255, 'February'),
(3, 'London', 8, 255, 'March'),
(4, 'London', 27, 255, 'April'),
(5, 'London', 29, 255, 'May'),
(6, 'London', 11, 255, 'June'),
(7, 'London', 6, 255, 'July '),
(8, 'London', -4, 255, 'August'),
(9, 'London', 23, 255, 'September'),
(10, 'London', 2, 255, 'October'),
(11, 'London', 3, 255, 'November'),
(12, 'London', 0, 255, 'December'),
(13, 'Belgrade', 12, 255, 'January'),
(14, 'Belgrade', 18, 255, 'February'),
(15, 'Belgrade', -3, 255, 'March'),
(16, 'Belgrade', 34, 255, 'April'),
(17, 'Belgrade', 4, 255, 'May'),
(18, 'Belgrade', 22, 255, 'June'),
(19, 'Belgrade', 3, 255, 'July '),
(20, 'Belgrade', 3, 255, 'August'),
(21, 'Belgrade', 14, 255, 'September'),
(22, 'Belgrade', 1, 255, 'October'),
(23, 'Belgrade', 23, 255, 'November'),
(24, 'Belgrade', 6, 255, 'December'),
(25, 'Paris', -2, 255, 'January'),
(26, 'Paris', 27, 255, 'February'),
(27, 'Paris', -3, 255, 'March'),
(28, 'Paris', -9, 255, 'April'),
(29, 'Paris', 22, 255, 'May'),
(30, 'Paris', 32, 255, 'June'),
(31, 'Paris', 7, 255, 'July '),
(32, 'Paris', 9, 255, 'August'),
(33, 'Paris', 10, 255, 'September'),
(34, 'Paris', 25, 255, 'October'),
(35, 'Paris', -3, 255, 'November'),
(36, 'Paris', -10, 255, 'December'),
(37, 'London', 2, 311, 'January'),
(38, 'London', 33, 311, 'February'),
(39, 'London', 24, 311, 'March'),
(40, 'London', 24, 311, 'April'),
(41, 'London', 6, 311, 'May'),
(42, 'London', 34, 311, 'June'),
(43, 'London', 34, 311, 'July '),
(44, 'London', 17, 311, 'August'),
(45, 'London', 17, 311, 'September'),
(46, 'London', 26, 311, 'October'),
(47, 'London', 17, 311, 'November'),
(48, 'London', -1, 311, 'December'),
(49, 'Belgrade', -1, 311, 'January'),
(50, 'Belgrade', -8, 311, 'February'),
(51, 'Belgrade', -9, 311, 'March'),
(52, 'Belgrade', 33, 311, 'April'),
(53, 'Belgrade', 3, 311, 'May'),
(54, 'Belgrade', 33, 311, 'June'),
(55, 'Belgrade', -8, 311, 'July '),
(56, 'Belgrade', 2, 311, 'August'),
(57, 'Belgrade', 29, 311, 'September'),
(58, 'Belgrade', 9, 311, 'October'),
(59, 'Belgrade', 34, 311, 'November'),
(60, 'Belgrade', 17, 311, 'December'),
(61, 'Paris', 14, 311, 'January'),
(62, 'Paris', 16, 311, 'February'),
(63, 'Paris', -10, 311, 'March'),
(64, 'Paris', 30, 311, 'April'),
(65, 'Paris', 18, 311, 'May'),
(66, 'Paris', 20, 311, 'June'),
(67, 'Paris', 31, 311, 'July '),
(68, 'Paris', -6, 311, 'August'),
(69, 'Paris', 23, 311, 'September'),
(70, 'Paris', 24, 311, 'October'),
(71, 'Paris', 2, 311, 'November'),
(72, 'Paris', 4, 311, 'December'),
(73, 'London', 21, 555, 'January'),
(74, 'London', 16, 555, 'February'),
(75, 'London', -8, 555, 'March'),
(76, 'London', -2, 555, 'April'),
(77, 'London', 28, 555, 'May'),
(78, 'London', 31, 555, 'June'),
(79, 'London', 3, 555, 'July '),
(80, 'London', 14, 555, 'August'),
(81, 'London', 7, 555, 'September'),
(82, 'London', 14, 555, 'October'),
(83, 'London', 7, 555, 'November'),
(84, 'London', 11, 555, 'December'),
(85, 'Belgrade', -4, 555, 'January'),
(86, 'Belgrade', 27, 555, 'February'),
(87, 'Belgrade', 32, 555, 'March'),
(88, 'Belgrade', 19, 555, 'April'),
(89, 'Belgrade', 18, 555, 'May'),
(90, 'Belgrade', 7, 555, 'June'),
(91, 'Belgrade', 30, 555, 'July '),
(92, 'Belgrade', 15, 555, 'August'),
(93, 'Belgrade', 8, 555, 'September'),
(94, 'Belgrade', 11, 555, 'October'),
(95, 'Belgrade', 27, 555, 'November'),
(96, 'Belgrade', 5, 555, 'December'),
(97, 'Paris', 14, 555, 'January'),
(98, 'Paris', 3, 555, 'February'),
(99, 'Paris', 34, 555, 'March'),
(100, 'Paris', 21, 555, 'April'),
(101, 'Paris', 4, 555, 'May'),
(102, 'Paris', 15, 555, 'June'),
(103, 'Paris', 10, 555, 'July '),
(104, 'Paris', 16, 555, 'August'),
(105, 'Paris', 21, 555, 'September'),
(106, 'Paris', -5, 555, 'October'),
(107, 'Paris', -9, 555, 'November'),
(108, 'Paris', 6, 555, 'December'),
(109, 'London', -8, 255, 'January'),
(110, 'London', 5, 255, 'February'),
(111, 'London', 1, 255, 'March'),
(112, 'London', 33, 255, 'April'),
(113, 'London', 10, 255, 'May'),
(114, 'London', 32, 255, 'June'),
(115, 'London', 29, 255, 'July '),
(116, 'London', 12, 255, 'August'),
(117, 'London', -4, 255, 'September'),
(118, 'London', 15, 255, 'October'),
(119, 'London', 0, 255, 'November'),
(120, 'London', 20, 255, 'December'),
(121, 'Belgrade', 14, 255, 'January'),
(122, 'Belgrade', -9, 255, 'February'),
(123, 'Belgrade', -10, 255, 'March'),
(124, 'Belgrade', 21, 255, 'April'),
(125, 'Belgrade', 9, 255, 'May'),
(126, 'Belgrade', 32, 255, 'June'),
(127, 'Belgrade', 27, 255, 'July '),
(128, 'Belgrade', 7, 255, 'August'),
(129, 'Belgrade', -10, 255, 'September'),
(130, 'Belgrade', 23, 255, 'October'),
(131, 'Belgrade', 34, 255, 'November'),
(132, 'Belgrade', 35, 255, 'December'),
(133, 'Paris', -6, 255, 'January'),
(134, 'Paris', 34, 255, 'February'),
(135, 'Paris', 35, 255, 'March'),
(136, 'Paris', -5, 255, 'April'),
(137, 'Paris', -6, 255, 'May'),
(138, 'Paris', -1, 255, 'June'),
(139, 'Paris', 21, 255, 'July '),
(140, 'Paris', 19, 255, 'August'),
(141, 'Paris', 1, 255, 'September'),
(142, 'Paris', -6, 255, 'October'),
(143, 'Paris', 35, 255, 'November'),
(144, 'Paris', 21, 255, 'December'),
(145, 'London', 35, 311, 'January'),
(146, 'London', 18, 311, 'February'),
(147, 'London', -4, 311, 'March'),
(148, 'London', -4, 311, 'April'),
(149, 'London', 29, 311, 'May'),
(150, 'London', 27, 311, 'June'),
(151, 'London', 9, 311, 'July '),
(152, 'London', 16, 311, 'August'),
(153, 'London', -7, 311, 'September'),
(154, 'London', 13, 311, 'October'),
(155, 'London', 14, 311, 'November'),
(156, 'London', 35, 311, 'December'),
(157, 'Belgrade', 3, 311, 'January'),
(158, 'Belgrade', 17, 311, 'February'),
(159, 'Belgrade', -7, 311, 'March'),
(160, 'Belgrade', -2, 311, 'April'),
(161, 'Belgrade', 8, 311, 'May'),
(162, 'Belgrade', 0, 311, 'June'),
(163, 'Belgrade', 34, 311, 'July '),
(164, 'Belgrade', 23, 311, 'August'),
(165, 'Belgrade', 8, 311, 'September'),
(166, 'Belgrade', -3, 311, 'October'),
(167, 'Belgrade', 14, 311, 'November'),
(168, 'Belgrade', 0, 311, 'December'),
(169, 'Paris', 33, 311, 'January'),
(170, 'Paris', 28, 311, 'February'),
(171, 'Paris', 17, 311, 'March'),
(172, 'Paris', 0, 311, 'April'),
(173, 'Paris', 3, 311, 'May'),
(174, 'Paris', 11, 311, 'June'),
(175, 'Paris', 19, 311, 'July '),
(176, 'Paris', 5, 311, 'August'),
(177, 'Paris', -5, 311, 'September'),
(178, 'Paris', 4, 311, 'October'),
(179, 'Paris', -9, 311, 'November'),
(180, 'Paris', 21, 311, 'December'),
(181, 'London', 4, 555, 'January'),
(182, 'London', 17, 555, 'February'),
(183, 'London', -3, 555, 'March'),
(184, 'London', 13, 555, 'April'),
(185, 'London', 25, 555, 'May'),
(186, 'London', 33, 555, 'June'),
(187, 'London', 5, 555, 'July '),
(188, 'London', 16, 555, 'August'),
(189, 'London', -2, 555, 'September'),
(190, 'London', 27, 555, 'October'),
(191, 'London', 32, 555, 'November'),
(192, 'London', 27, 555, 'December'),
(193, 'Belgrade', -10, 555, 'January'),
(194, 'Belgrade', 35, 555, 'February'),
(195, 'Belgrade', 33, 555, 'March'),
(196, 'Belgrade', 31, 555, 'April'),
(197, 'Belgrade', -8, 555, 'May'),
(198, 'Belgrade', -10, 555, 'June'),
(199, 'Belgrade', 19, 555, 'July '),
(200, 'Belgrade', 15, 555, 'August'),
(201, 'Belgrade', 15, 555, 'September'),
(202, 'Belgrade', 8, 555, 'October'),
(203, 'Belgrade', -10, 555, 'November'),
(204, 'Belgrade', 26, 555, 'December'),
(205, 'Paris', 6, 555, 'January'),
(206, 'Paris', 17, 555, 'February'),
(207, 'Paris', 6, 555, 'March'),
(208, 'Paris', 17, 555, 'April'),
(209, 'Paris', 11, 555, 'May'),
(210, 'Paris', 11, 555, 'June'),
(211, 'Paris', -3, 555, 'July '),
(212, 'Paris', 13, 555, 'August'),
(213, 'Paris', 33, 555, 'September'),
(214, 'Paris', 18, 555, 'October'),
(215, 'Paris', 5, 555, 'November'),
(216, 'Paris', 27, 555, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `id` int(11) NOT NULL,
  `contract` text NOT NULL,
  `mac_address` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`id`, `contract`, `mac_address`) VALUES
(1, '255', 1),
(2, '311', 1),
(3, '555', 1);

-- --------------------------------------------------------

--
-- Table structure for table `growth`
--

CREATE TABLE `growth` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `value` int(11) NOT NULL,
  `contract` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `growth`
--

INSERT INTO `growth` (`id`, `date`, `value`, `contract`) VALUES
(1, '0000-00-00', 97, 255),
(2, '2020-01-01', 39, 255),
(3, '2020-01-02', 158, 255),
(4, '2020-01-03', 223, 255),
(5, '2020-01-04', -40, 255),
(6, '2020-01-05', 64, 255),
(7, '2020-01-06', 44, 255),
(8, '2020-01-07', -11, 255),
(9, '2020-01-08', 15, 255),
(10, '2020-01-09', 32, 255),
(11, '2020-01-10', 242, 255),
(12, '2020-01-11', 251, 255),
(13, '2020-01-12', 72, 255),
(14, '2020-01-13', 53, 255),
(15, '2020-01-14', 160, 255),
(16, '2020-01-15', 125, 255),
(17, '2020-01-16', -1, 255),
(18, '2020-01-17', 43, 255),
(19, '2020-01-18', 104, 255),
(20, '2020-01-19', 116, 255),
(21, '2020-01-20', 156, 255),
(22, '2020-01-21', -28, 255),
(23, '2020-01-22', 247, 255),
(24, '2020-01-23', -42, 255),
(25, '2020-01-24', 121, 255),
(26, '2020-01-25', 169, 255),
(27, '2020-01-26', 194, 255),
(28, '2020-01-27', 199, 255),
(29, '2020-01-28', 276, 255),
(30, '2020-01-29', 133, 255),
(31, '2020-01-30', -15, 255),
(32, '2020-01-31', 62, 255),
(33, '0000-00-00', 98, 311),
(34, '2020-01-01', 216, 311),
(35, '2020-01-02', 191, 311),
(36, '2020-01-03', 55, 311),
(37, '2020-01-04', -32, 311),
(38, '2020-01-05', 236, 311),
(39, '2020-01-06', -4, 311),
(40, '2020-01-07', -7, 311),
(41, '2020-01-08', 180, 311),
(42, '2020-01-09', 38, 311),
(43, '2020-01-10', -49, 311),
(44, '2020-01-11', -3, 311),
(45, '2020-01-12', 34, 311),
(46, '2020-01-13', 102, 311),
(47, '2020-01-14', 249, 311),
(48, '2020-01-15', 42, 311),
(49, '2020-01-16', 63, 311),
(50, '2020-01-17', 192, 311),
(51, '2020-01-18', 287, 311),
(52, '2020-01-19', 82, 311),
(53, '2020-01-20', 162, 311),
(54, '2020-01-21', 180, 311),
(55, '2020-01-22', 169, 311),
(56, '2020-01-23', 282, 311),
(57, '2020-01-24', 172, 311),
(58, '2020-01-25', 112, 311),
(59, '2020-01-26', 274, 311),
(60, '2020-01-27', 43, 311),
(61, '2020-01-28', 76, 311),
(62, '2020-01-29', 207, 311),
(63, '2020-01-30', 104, 311),
(64, '2020-01-31', 221, 311),
(65, '0000-00-00', 194, 555),
(66, '2020-01-01', 293, 555),
(67, '2020-01-02', 170, 555),
(68, '2020-01-03', 4, 555),
(69, '2020-01-04', -33, 555),
(70, '2020-01-05', -10, 555),
(71, '2020-01-06', 242, 555),
(72, '2020-01-07', 243, 555),
(73, '2020-01-08', 164, 555),
(74, '2020-01-09', 198, 555),
(75, '2020-01-10', 217, 555),
(76, '2020-01-11', 53, 555),
(77, '2020-01-12', 193, 555),
(78, '2020-01-13', 10, 555),
(79, '2020-01-14', 236, 555),
(80, '2020-01-15', 230, 555),
(81, '2020-01-16', 78, 555),
(82, '2020-01-17', -49, 555),
(83, '2020-01-18', 53, 555),
(84, '2020-01-19', 195, 555),
(85, '2020-01-20', 241, 555),
(86, '2020-01-21', 81, 555),
(87, '2020-01-22', 191, 555),
(88, '2020-01-23', 147, 555),
(89, '2020-01-24', 68, 555),
(90, '2020-01-25', 229, 555),
(91, '2020-01-26', 296, 555),
(92, '2020-01-27', 36, 555),
(93, '2020-01-28', -41, 555),
(94, '2020-01-29', -33, 555),
(95, '2020-01-30', 40, 555),
(96, '2020-01-31', 119, 555),
(97, '0000-00-00', 12, 255),
(98, '2020-01-01', 185, 255),
(99, '2020-01-02', 145, 255),
(100, '2020-01-03', 288, 255),
(101, '2020-01-04', 5, 255),
(102, '2020-01-05', -37, 255),
(103, '2020-01-06', 47, 255),
(104, '2020-01-07', 188, 255),
(105, '2020-01-08', 249, 255),
(106, '2020-01-09', 178, 255),
(107, '2020-01-10', 227, 255),
(108, '2020-01-11', -40, 255),
(109, '2020-01-12', 54, 255),
(110, '2020-01-13', 258, 255),
(111, '2020-01-14', 74, 255),
(112, '2020-01-15', -28, 255),
(113, '2020-01-16', 272, 255),
(114, '2020-01-17', 102, 255),
(115, '2020-01-18', 251, 255),
(116, '2020-01-19', 298, 255),
(117, '2020-01-20', 190, 255),
(118, '2020-01-21', 291, 255),
(119, '2020-01-22', 0, 255),
(120, '2020-01-23', 266, 255),
(121, '2020-01-24', 294, 255),
(122, '2020-01-25', 88, 255),
(123, '2020-01-26', 62, 255),
(124, '2020-01-27', 259, 255),
(125, '2020-01-28', -35, 255),
(126, '2020-01-29', 282, 255),
(127, '2020-01-30', 20, 255),
(128, '2020-01-31', -49, 255),
(129, '0000-00-00', 55, 311),
(130, '2020-01-01', 96, 311),
(131, '2020-01-02', 214, 311),
(132, '2020-01-03', 49, 311),
(133, '2020-01-04', 288, 311),
(134, '2020-01-05', 53, 311),
(135, '2020-01-06', 222, 311),
(136, '2020-01-07', 112, 311),
(137, '2020-01-08', -9, 311),
(138, '2020-01-09', 226, 311),
(139, '2020-01-10', -1, 311),
(140, '2020-01-11', 231, 311),
(141, '2020-01-12', 140, 311),
(142, '2020-01-13', 189, 311),
(143, '2020-01-14', 64, 311),
(144, '2020-01-15', 166, 311),
(145, '2020-01-16', 117, 311),
(146, '2020-01-17', 33, 311),
(147, '2020-01-18', 144, 311),
(148, '2020-01-19', 257, 311),
(149, '2020-01-20', 41, 311),
(150, '2020-01-21', 277, 311),
(151, '2020-01-22', 163, 311),
(152, '2020-01-23', 67, 311),
(153, '2020-01-24', 115, 311),
(154, '2020-01-25', 190, 311),
(155, '2020-01-26', 36, 311),
(156, '2020-01-27', 10, 311),
(157, '2020-01-28', 159, 311),
(158, '2020-01-29', 148, 311),
(159, '2020-01-30', 7, 311),
(160, '2020-01-31', 36, 311),
(161, '0000-00-00', 251, 555),
(162, '2020-01-01', 115, 555),
(163, '2020-01-02', 123, 555),
(164, '2020-01-03', 281, 555),
(165, '2020-01-04', -19, 555),
(166, '2020-01-05', 14, 555),
(167, '2020-01-06', 292, 555),
(168, '2020-01-07', 7, 555),
(169, '2020-01-08', -37, 555),
(170, '2020-01-09', 171, 555),
(171, '2020-01-10', 99, 555),
(172, '2020-01-11', 139, 555),
(173, '2020-01-12', 210, 555),
(174, '2020-01-13', 56, 555),
(175, '2020-01-14', 105, 555),
(176, '2020-01-15', 19, 555),
(177, '2020-01-16', 130, 555),
(178, '2020-01-17', 176, 555),
(179, '2020-01-18', 272, 555),
(180, '2020-01-19', -33, 555),
(181, '2020-01-20', 228, 555),
(182, '2020-01-21', 195, 555),
(183, '2020-01-22', 36, 555),
(184, '2020-01-23', 217, 555),
(185, '2020-01-24', 208, 555),
(186, '2020-01-25', 261, 555),
(187, '2020-01-26', 232, 555),
(188, '2020-01-27', -41, 555),
(189, '2020-01-28', 126, 555),
(190, '2020-01-29', -49, 555),
(191, '2020-01-30', 133, 555),
(192, '2020-01-31', 267, 555);

-- --------------------------------------------------------

--
-- Table structure for table `hgw`
--

CREATE TABLE `hgw` (
  `id` int(11) NOT NULL,
  `contract` text NOT NULL,
  `bitrate` text NOT NULL,
  `RSS` text NOT NULL,
  `network` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hgw`
--

INSERT INTO `hgw` (`id`, `contract`, `bitrate`, `RSS`, `network`) VALUES
(1, '255', '{\"min\":1,\"max\":16,\"avg\":8.5,\"last\":1}', '{\"min\":0,\"max\":25,\"avg\":12.5,\"last\":35}', '{\"min\":2,\"max\":29,\"avg\":15.5,\"last\":32}'),
(2, '311', '{\"min\":0,\"max\":33,\"avg\":16.5,\"last\":34}', '{\"min\":12,\"max\":29,\"avg\":20.5,\"last\":15}', '{\"min\":9,\"max\":19,\"avg\":14,\"last\":9}'),
(3, '555', '{\"min\":2,\"max\":25,\"avg\":13.5,\"last\":5}', '{\"min\":10,\"max\":32,\"avg\":21,\"last\":4}', '{\"min\":3,\"max\":29,\"avg\":16,\"last\":19}');

-- --------------------------------------------------------

--
-- Table structure for table `mac_address`
--

CREATE TABLE `mac_address` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mac_address`
--

INSERT INTO `mac_address` (`id`, `address`) VALUES
(1, '2C.AB.A4.0A.53.FC'),
(2, '8E-5C-51-DE-34-C7');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'matei', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atmosphere`
--
ALTER TABLE `atmosphere`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `avg_sales`
--
ALTER TABLE `avg_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `avg_temp`
--
ALTER TABLE `avg_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `growth`
--
ALTER TABLE `growth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hgw`
--
ALTER TABLE `hgw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mac_address`
--
ALTER TABLE `mac_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atmosphere`
--
ALTER TABLE `atmosphere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `avg_sales`
--
ALTER TABLE `avg_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT for table `avg_temp`
--
ALTER TABLE `avg_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `growth`
--
ALTER TABLE `growth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `hgw`
--
ALTER TABLE `hgw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mac_address`
--
ALTER TABLE `mac_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
