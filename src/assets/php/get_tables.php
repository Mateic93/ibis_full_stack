<?php
include "autoload.php";
// inserting headers here, instaed of in .htaccess file
include "scripts/headers.php";
$data = json_decode(file_get_contents("php://input"));
$request = $data->request;

$instance = Database::getInstance();
$db = $instance->getConnection();

$table = htmlspecialchars(strip_tags($data->tables));
// $table = "hgw";

$contract = htmlspecialchars(strip_tags($data->contract));

$sql = "select * from $table where contract = $contract";
$result = $db->query($sql);
$send_data = [];
foreach($result as $res){
    $send_data = $res;
}


echo json_encode(array("data" => $send_data));
die();
