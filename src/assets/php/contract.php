<?php
  include "autoload.php";
  // inserting headers here, instaed of in .htaccess file
  include "scripts/headers.php";

  // read raw POST data (JSON data) | not available with enctype="multipart/form-data"
  $data = json_decode(file_get_contents("php://input"));
//   echo json_encode(array("username" => "test"));
  $request = $data->request;
  
  if($request == "contract_check" && $data->contract !== ""){
      $instance = Database::getInstance();
      $db = $instance->getConnection();
      $contract = htmlspecialchars(strip_tags($data->contract));
      $sql = "SELECT * FROM `contracts` WHERE contract like '".$contract ."%'";
      $result = $db->query($sql);
      // $number = mysqli_num_rows($result);
      $contracts = [];
      if($result){
        foreach($result as $res){
            array_push($contracts, $res["contract"]);
        }
        echo json_encode(array("contracts" => $contracts));
      }else {
        echo json_encode(array("message" => "there's nothing here"));
      }
      
  }
